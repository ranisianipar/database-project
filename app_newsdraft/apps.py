from django.apps import AppConfig


class AppNewsdraftConfig(AppConfig):
    name = 'app_newsdraft'
