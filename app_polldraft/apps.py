from django.apps import AppConfig


class AppPolldraftConfig(AppConfig):
    name = 'app_polldraft'
