from django.db import models

# Create your models here.

class daftar_polling(models.Model):
	kode = models.IntegerField(primary_key=True)
	deskripsi = models.CharField(max_length=50)
	polling_start = models.DateField()
	polling_end = models.DateField()
	url = models.CharField(max_length=100)
	responden = models.IntegerField()
	