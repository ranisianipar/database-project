"""TK4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import app_create.urls as app_create
import app_login.urls as login
import app_signup.urls as signup
import app_profile.urls as profile
import app_newsdraft.urls as newsdraft
import app_polldraft.urls as polldraft

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', include(login, namespace='login')),
    url(r'^create/', include(app_create, namespace='create')),
    url(r'^signup/', include(signup, namespace='signup')),
     url(r'^profile/', include(profile, namespace='profile')),
    url(r'^newsdraft/', include(newsdraft, namespace='newsdraft')),
    url(r'^polldraft/', include(polldraft, namespace='polldraft')),
    url(r'^$', RedirectView.as_view(url='login/',permanent='True')) #kalo perlu
]
