from django.conf.urls import url
from .views import index, berita, add_berita, pollingBiasa, pollingBerita,add_polling_biasa, add_polling_berita
#url for app
app_name = 'message'
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^berita', berita,name='berita'),
    url(r'^add_berita', add_berita, name='add_berita'),
    url(r'^polling_biasa', pollingBiasa,name='polling_biasa'),
    url(r'^add_polling_biasa', add_polling_biasa, name='add_polling_biasa'),
    url(r'^polling_berita', pollingBerita,name='polling_berita'),
    url(r'^add_polling_berita', add_polling_berita, name='add_polling_berita'),
]
