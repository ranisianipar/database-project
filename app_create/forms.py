from django import forms

class Berita_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    judul_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan judul'
    }
    url_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan url'
    }
    topik_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan topik'
    }
    tag_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan tag'
    }
    jumKat_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan jumlah kata'
    }

    judul = forms.CharField(label='Judul', required=True, max_length=100, widget=forms.TextInput(attrs=judul_attrs))
    url = forms.CharField(label='URL', required=True, max_length=50, widget=forms.TextInput(attrs=url_attrs))
    topik = forms.CharField(label='Topik', required=True, max_length=100, widget=forms.TextInput(attrs=topik_attrs))
    jumlahKata = forms.IntegerField(label='Jumlah Kata', required=True, widget=forms.TextInput(attrs=jumKat_attrs))
    tag = forms.CharField(label='Tag', required=True, max_length=50, widget=forms.TextInput(attrs=tag_attrs))

class PollingBiasa_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    deskripsi_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan deskripsi'
    }
    waktu_mulai_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan waktu mulai'
    }
    waktu_selesai_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan waktu selesai'
    }
    pertanyaan_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan pertanyaan'
    }

    deskripsi = forms.CharField(label='Deskripsi', required=True, max_length=100, widget=forms.TextInput(attrs=deskripsi_attrs))
    waktu_mulai = forms.CharField(label='Waktu Mulai', required=True, max_length=50, widget=forms.TextInput(attrs=waktu_mulai_attrs))
    waktu_selesai = forms.CharField(label='Waktu Selesai', required=True, max_length=100, widget=forms.TextInput(attrs=waktu_selesai_attrs))
    pertanyaan = forms.CharField(label='Pertanyaan', required=True, max_length=100, widget=forms.TextInput(attrs=pertanyaan_attrs))

class PollingBerita_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    url_berita_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan deskripsi'
    }
    waktu_mulai_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan waktu mulai'
    }
    waktu_selesai_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan waktu selesai'
    }
    pertanyaan_attrs = {
        'type': 'text',
        'class': 'surat-form-input',
        'placeholder':'Masukan pertanyaan'
    }

    url_berita = forms.CharField(label='URL Berita', required=True, max_length=50, widget=forms.TextInput(attrs=url_berita_attrs))
    waktu_mulai = forms.CharField(label='Waktu Mulai', required=True, max_length=50, widget=forms.TextInput(attrs=waktu_mulai_attrs))
    waktu_selesai = forms.CharField(label='Waktu Selesai', required=True, max_length=100, widget=forms.TextInput(attrs=waktu_selesai_attrs))
    pertanyaan = forms.CharField(label='Pertanyaan', required=True, max_length=100, widget=forms.TextInput(attrs=pertanyaan_attrs))

