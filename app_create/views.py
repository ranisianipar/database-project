from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Berita_Form, PollingBiasa_Form, PollingBerita_Form
from .models import Berita, PollingBiasa, PollingBerita

# Create your views here.
response = {}
def index(request):
    berita = Berita.objects.all()
    response['berita'] = berita
    html = 'app_create/app_create.html'
    return render(request, html, response)

def berita(request):
    response['berita_form'] = Berita_Form
    html = 'app_create/more/berita.html'
    return render(request, html, response)

def pollingBiasa(request):
    response['pollingBiasa_form'] = PollingBiasa_Form
    html = 'app_create/more/polling_biasa.html'
    return render(request, html, response)

def pollingBerita(request):
    response['pollingBerita_form'] = PollingBerita_Form
    html = 'app_create/more/polling_berita.html'
    return render(request, html, response)

def add_berita(request):
    form = Berita_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['judul'] = request.POST['judul']
        response['url'] = request.POST['url']
        response['topik'] = request.POST['topik']
        response['jumlahKata'] = request.POST['jumlahKata']
        berita = Berita(judul=response['judul'],url=response['url'],topik=response['topik'],jumlahKata=50)
        berita.save()
        return HttpResponseRedirect('/create/berita/')
    else:
        return HttpResponseRedirect('/create/berita/')

def add_polling_biasa(request):
    form = Berita_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['deskripsi'] = request.POST['deskripsi']
        response['waktu_mulai'] = request.POST['waktu_mulai']
        response['waktu_selesai'] = request.POST['waktu_selesai']
        response['pertanyaan'] = request.POST['pertanyaan']
        berita = Berita(judul=response['judul'],url=response['url'],topik=response['topik'])
        berita.save()
        return HttpResponseRedirect('/create/polling_biasa/')
    else:
        return HttpResponseRedirect('/create/polling_biasa/')

def add_polling_berita(request):
    form = Berita_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['url_berita'] = request.POST['url_berita']
        response['waktu_mulai'] = request.POST['waktu_mulai']
        response['waktu_selesai'] = request.POST['waktu_selesai']
        response['pertanyaan'] = request.POST['pertanyaan']
        berita = Berita(judul=response['judul'],url=response['url'],topik=response['topik'])
        berita.save()
        return HttpResponseRedirect('/create/polling_berita/')
    else:
        return HttpResponseRedirect('/create/polling_berita/')
