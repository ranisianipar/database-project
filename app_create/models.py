from django.db import models

# Create your models here.
class Berita(models.Model):
	judul = models.CharField(max_length=100)
	url = models.CharField(max_length=50)
	topik = models.CharField(max_length=100)
	jumlahKata = models.IntegerField()
	tag = models.CharField(max_length=50)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'berita'

class PollingBiasa(models.Model):
	deskripsi = models.CharField(max_length=100)
	waktu_mulai = models.CharField(max_length=50)
	waktu_selesai = models.CharField(max_length=100)
	pertanyaan = models.CharField(max_length=100)

	class Meta:
		db_table = 'polling_biasa'
	
class PollingBerita(models.Model):
	url_berita = models.CharField(max_length=50)
	waktu_mulai = models.CharField(max_length=50)
	waktu_selesai = models.CharField(max_length=100)
	pertanyaan = models.CharField(max_length=100)

	class Meta:
		db_table = 'polling_berita'