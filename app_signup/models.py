from django.db import models

# Create your models here.

class Pengguna(models.Model):
	username = models.CharField(max_length = 20, primary_key= True)
	password = models.CharField(max_length = 20)
	role = models.CharField(max_length = 20)
	
	class Meta:
		db_table = "Pengguna"
		
class Narasumber(models.Model):
	username = models.CharField(max_length = 20, primary_key= True)
	idNum = models.CharField(max_length=10) # anggepannya npm
	name = models.CharField(max_length=50)
	birthdate = models.CharField(max_length=10) #format: DD/MM/YYYY
	birthplace = models.CharField(max_length=20)
	email = models.EmailField()
	phone = models.CharField(max_length = 15)
	
	# tergantung role nya
	# status = models.CharField(max_length= 20, blank=True)
	# idUniv = models.IntegerField(blank=True) #ada 50 univ
	
	class Meta:
		db_table = "Narasumber"
		
		
# cekkk, perlu pake forms.py?