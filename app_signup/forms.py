from django import forms

# Create your models here.

class PenggunaForm(forms.Form):
	username = forms.CharField(max_length = 20)
	password = forms.CharField(max_length = 20)
	role = forms.CharField(max_length = 20)
	
	class Meta:
		db_table = "Pengguna"
		
class NarasumberForm(forms.Form):
	idNum = forms.CharField(max_length=15) # anggepannya npm
	name = forms.CharField(max_length=50)
	# birthdate = forms.DateField()
	birthplace = forms.CharField(max_length=30)
	email = forms.EmailField()
	phone = forms.CharField(max_length = 15)
	
	# tergantung role nya
	# status = forms.CharField(max_length= 20)
	# idUniv = forms.IntegerField() #ada 50 univ
		
		
# cekkk, perlu pake forms.py?