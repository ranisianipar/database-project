from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseNotFound
from .models import Pengguna, Narasumber
from .forms import PenggunaForm, NarasumberForm

# Create your views here.
response = {}

def index(request):	
	html = 'signup.html'
	response['author'] = 'Kelompok 8'
	return render(request, html, response)

def regist(request):
	userform = PenggunaForm(request.POST or None)
	narsumform = NarasumberForm(request.POST or None)
	print("VALIDATOR: =========== "+str(userform.is_valid())+" narsum "+str(narsumform.is_valid()))
	if(request.method == 'POST' and userform.is_valid() and narsumform.is_valid()):
		response['username'] = request.POST['username']
		response['password'] = request.POST['password']
		response['idNum'] = request.POST['idNum']
		response['role'] = request.POST['role']
		response['name'] = request.POST['name']
		response['email'] = request.POST['email']
		response['phone'] = request.POST['phone']
		response['birthdate'] = request.POST['birthdate']
		response['birthplace'] = request.POST['birthplace']

		#ngesave ke database
		user = Pengguna(username=response['username'],password=response['password'],role=response['role'])
		narsum = Narasumber(username=response['username'],idNum=response['idNum'], name=response['name'], birthplace=response['birthplace'], birthdate=response['birthdate'], phone=response['phone'], email=response['email'])

		user.save()
		narsum.save()
		#ga pake additional atrribute

		return HttpResponseRedirect('/login/')
	return HttpResponseRedirect('/signup/')
