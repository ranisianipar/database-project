from django.conf.urls import url
from .views import index,regist

#url for app, add your URL Configuration

urlpatterns = [
url(r'^$', index, name='index'),
url(r'^index', index, name='index'),
url(r'^regist/$', regist, name='regist'),
]
