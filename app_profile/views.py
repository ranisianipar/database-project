from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

role = 'Mahasiswa'
username = 'ini_aku'
nomor_id = '123456789'
nama = 'Yoshi Noya'
tempat_lahir = 'Fasilkom UI'
tanggal_lahir = '30 Februari 1999'
email = 'yoshi@noya.id'
nomor_hp = '0811992299'
status = 'Aktif'

bio_dict = [{'subject' : 'Role', 'value' : '  ' + role},\
			{'subject' : 'Username', 'value' : ' ' + username},\
			{'subject' : 'ID Number', 'value' : ' ' +nomor_id},\
			{'subject' : 'Name', 'value' : ' ' +nama},\
			{'subject' : 'Birthplace', 'value' : ' ' +tempat_lahir},\
			{'subject' : 'Birthdate', 'value' : ' ' +tanggal_lahir},\
			{'subject' : 'E-mail', 'value' : ' ' +email},\
			{'subject' : 'Phone Number', 'value' : ' ' +nomor_hp},\
			{'subject' : 'Status', 'value' : ' ' +status}]

def index(request):
	response = {'bio_dict' : bio_dict}
	return render(request, 'app_profile.html', response)