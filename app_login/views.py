from django.shortcuts import render

# Create your views here.
response = {}

def index(request):
	response['status'] = False #value = True when user has already logged in
	html = 'login.html'
	return render(request,html,response)
